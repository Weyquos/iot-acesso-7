var pin = new Array();
var modo = 0;
var configInput = '#idIOT';
var user = 0;
var call = null

//================================================ Teclado STR ================================================//

function tecladoConfig(value){
    var conteudo = $(configInput).val();
    if(value == -1){
        var inputString = $(configInput).val();
        var shortenedString = inputString.substr(0,(inputString.length -1));
        $(configInput).val(shortenedString);
    }else{
        if(conteudo.length == 2 && configInput != '#idIOT'){
            $(configInput).val(conteudo +"");
        }else{
            $(configInput).val(conteudo + value + "");
        }
    }
}

function updateApp(){
    document.getElementById('loading').style.display = 'block';
    $.getJSON('/att_app',  function(data) {
        if(data == 0){
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;margin-top:80px;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
        }else{
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;margin-top:80px;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
        }
    });
}


function deletarBanco(){
    document.getElementById('loading').style.display = 'block';
    $.getJSON('/del_banco',  function(data) {
        if(data == 0){
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;margin-top:80px;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Deletando e Reiniciando");}, 4000);
        }else{
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;margin-top:80px;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Deletando e Reiniciando");}, 4000);
        }
    });
}

//Altera campo no qual o teclado insere os valores na tela de configuração
function changeConfigInput(input){
    configInput = input;
}

function calc(valor){
    var j = pin.length;
    if(valor == -1){
        if(j>0){
            pin.pop();
            $(".visor_valor").val(pin.join(""));
        }else {
            if(modo != 1){
                $.getJSON('/leitura',  function(data) {
                });
                modo = 0;
                $('#aviso').html('');
            }
        }
    }else if(valor == -2 && j==4){
        if(modo == -1){
            validarEncerramento(pin.join("").toString());
            resetarTeclado();
        }else if(modo == 0){
            entrar(pin.join("").toString());
            resetarTeclado();
        }else if(modo == 2){
            validarPinAdmin(pin.join("").toString());
            resetarTeclado();
        }else if(modo == 3){
            validatePinAgendamento(pin.join("").toString());
            resetarTeclado();
        }else{
            validarPinCadastro(pin.join("").toString());
            resetarTeclado();
        }
    }else{
        if(modo == 4 && valor == -2){
            validarPinCadastroFuncionario(pin.join("").toString());
            resetarTeclado();
        }else if(j<4 && valor != -2){
            pin.push(valor);
            $(".visor_valor").val(pin.join(""));
        }
    }
}

function resetarTeclado(){
    pin = [];
    modo = 0;
    $('.visor_valor').val('');
}
//================================================ Teclado END ================================================//

//================================================ Configuracoes STR ================================================//

//Exibe painel de configurações do dispositivo após validação da senha de admin
function validarPinAdmin(pin){
    $.getJSON('/_validate_pin_admin', {pin:pin} ,function(data) {
        if(data != 0){
            //Chamar função para abrir porta, exibe "Bem Vindo, Nome" e abre a porta
             document.getElementById('myModal').style.display = 'block';
             $('#aviso').html('');
        }else{
            //Informar que o PIN é invalido
            $('#aviso').html("Pin incorreto!");
            setTimeout(function() {$('#aviso').html('');}, 4000);
        }
    });
}

//Reinicia o dispositivo
function reboot(){
    $.getJSON('/reboot',  function(data) {
    });
}

//Salva as alterações feitas na tela de configuração
function saveChanges(){
    //Update do tempo de abertura da porta
    if($('#hOpen').val() != '' || $('#mOpen').val() != '' || $('#sOpen').val() != ''){
        var time = 0;

        if($('#hOpen').val() != '' && $('#hOpen').val() != '0'){
            time += parseInt($('#hOpen').val()) * 3600;
        }

        if($('#mOpen').val() != '' && $('#mOpen').val() != '0'){
            time += parseInt($('#mOpen').val()) * 60;
        }

        if($('#sOpen').val() != '' && $('#sOpen').val() != '0'){
            time += parseInt($('#sOpen').val());
        }

        $.getJSON('/changeDoorTime', {time:time},  function(data) {
            $('#msgAbertura').html('Tempo de abertura da porta atualizado com sucesso!');
        });
    }

    //Update do horario de atualização automatica
    if($('#hUpdate').val() != '' || $('#mUpdate').val() != '' ){

    }

    //Update iotID
    if($('#idIOT').val() != ''){
        $.getJSON('/changeIOT', {iot:$('#idIOT').val()},  function(data) {
            $('#msgIOT').html('N° do IOT alterado com sucesso!');
            setTimeout(function() {location.reload();}, 4000);
        });
    }

    setTimeout(function() {$('#msgAbertura').html('');}, 4000);
    setTimeout(function() {document.getElementById('myModal').style.display = "none";}, 4000);

}

//================================================ Configuracoes END ================================================//

//================================================ Acesso STR ================================================//

//Caso exista conexão com o leitor, inicia-se o processo de update do leitor biometrico
function update(){
    closeNav();
    document.getElementById('loading').style.display = 'block';
    $.getJSON('/_update',  function(data) {
        if(data == 0){
//            setTimeout(function() {$('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;' class='btn btn-danger btn-circle'><i class='far fa-times-circle fa-5x' aria-hidden='true'></i></button></div>");}, 1000);
//            setTimeout(function() {$('#updatemsg').html("Falha ao conectar com o leitor");}, 1000);
//            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 4000);
//            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
//            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
        }else{
            $('#updateStatus').html("<div class='col-md-12' style='text-align:center;'><button type='button' style='width:120px;height:120px;border-radius:50%;' class='btn btn-soccess btn-circle'><i class='far fa-check-circle fa-5x' aria-hidden='true'></i></button></div>");
            $('#updatemsg').html("Concluido");
            setTimeout(function() {document.getElementById('loading').style.display = 'none';}, 3000);
            setTimeout(function() {$('#updateStatus').html("<div class='loader'></div>");}, 4000);
            setTimeout(function() {$('#updatemsg').html("Atualizando");}, 4000);
        }
    });
}

//Valida a permissão do pin informado. Caso possua permissão a porta é liberada
function entrar(pin){
    $.getJSON('/_validate_pin', {pin:pin} ,function(data) {
        if(data['result'] == 4){
            $('#aviso').html(data['nome']);
            $('#aviso2').html(data['msg']);
            if(data['permissao'] == 1){
                $.getJSON('/Fechar', function(data) {
                    setTimeout(function() {$('#aviso').html('');}, 4000);
                    setTimeout(function() {$('#aviso2').html('');}, 4000);
                });
            }else{
                setTimeout(function() {$('#aviso').html('');}, 4000);
                setTimeout(function() {$('#aviso2').html('');}, 4000);
            }
        }else if(data['result'] == 3){
            $('#aviso').html("Bem Vindo " + data['nome'] + "!");
            $.getJSON('/Fechar', function(data) {
                setTimeout(function() {$('#aviso').html('');}, 4000);
            });
        }else if(data['result'] == 2){
            $('#aviso').html("Bem Vindo " + data['nome'] + "!");
            var nomeU = data['nome'];
            $.getJSON('/_inciar_agendamento', {cod:data['cod']} ,function(data) {
                if(data['success'] == 1){
                    if(data['status'] == 0){
                        iniciarUso(nomeU);
                    }
                    $.getJSON('/Fechar', function(data) {
                         setTimeout(function() {$('#aviso').html('');}, 4000);
                    });
                }else{
                    setTimeout(function() {$('#aviso').html('Aguarde até pelo menos 5 minutos antes de seu agendamento para iniciar');}, 4000);
                    setTimeout(function() {$('#aviso').html('');}, 8000);
                }
            });
        }else if(data['result'] == 1){
            //Chamar função para abrir porta, exibe "Bem Vindo, Nome" e abre a porta
            $('#aviso').html("Bem Vindo " + data['nome'] + "!");
            $.getJSON('/Fechar', function(data) {
                 setTimeout(function() {$('#aviso').html('');}, 4000);
            });
        }else{
            //Informar que o PIN é invalido
            $('#aviso').html("Pin incorreto!");
            setTimeout(function() {$('#aviso').html('');}, 4000);
        }
    });
}

//Aciona a campainha para informar a chegada de um visitante
function Campainha(){
    $('#aviso').html("Notificação recebida!");
    setTimeout(function() {$('#aviso').html('');}, 4000);
    $.getJSON('/_campainha', function(data) {

    });
}

//================================================ Acesso END ================================================//


//================================================ UI STR ================================================//

// Exibe menu lateral
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

// Oculta menu lateral
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

// Apagas as mensagens da tela
function clearAvisos(){
    setTimeout(function() {$('#aviso3').html('');}, 4000);
    setTimeout(function() {$('#aviso2').html('');}, 4000);
    setTimeout(function() {$('#aviso').html('');}, 4000);
}

function sleep(millisecondsToWait){
    var now = new Date().getTime();
    while (new Date().getTime() < now + millisecondsToWait){

    }
}

function erroCadastro(msg){
    $('#aviso').html(msg);
    setTimeout(function() {$('#aviso').html('');}, 4000);
    $.getJSON('/leitura',  function(data) {
    });
}

//================================================ UI END ================================================//


//================================================ Biometria STR ================================================//

function CancelarBio(){
    $('#etapaCadastro').html('Pocisione o dedo sobre o leitor para confirmar.');
    $.getJSON('/cancelar_bio',  function(data) {

    });
}

//----------------------------------------------- Usuario STR -----------------------------------------------//

//Coloca o leitor biometrico no modo de cadastro
function Cadastrar(){
    $.getJSON('/cadastro',  function(data) {
        if(data == 1){
             closeNav();
            modo = 1;
            $('#aviso').html('Informe um Pin para prosseguir.');
        }else{
            closeNav();
            $('#aviso').html('O dispositivo de leitura esta atualizando, tente novamente mais tarde.');
        }
    });
}

// sleep time expects milliseconds
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

//Valida se o pin informado é valido para o cadastro biometrico
function validarPinCadastro(pin){
    $.getJSON('/_validate_pin_cadastro', {pin:pin} ,function(data) {
        if(data != 0 && data != -1 && data != null){
            var iotType = data['type'];
            user = data['cod'];
            $('#aviso').html("Iniciando cadastro para: <b> " + data['nome'] + "</b>");
             $.getJSON('/_count_digitais', {user:user}, function(data) {
                if(data != -1){
                    digital = data;
                    $('#aviso2').append('<br>Digitais Cadastradas: '+data+'/2.');
                    if(data == 2){
                        if(iotType == 1){
                            $('#aviso3').html('Quantidade maxima de digitais cadastradas.');
                        }else if(iotType == 2){
                            $('#aviso').html('Quantidade maxima de digitais cadastradas.');
                        }
                        clearAvisos();
                        return;
                    }else{
                        $('#etapaCadastro').html('Coloque o dedo sobre o leitor');
                        setTimeout(function() {document.getElementById('cadastramento').style.display = 'block';}, 1000);
                        $.getJSON('/_read_finger0', function(dataf0) {
                            if(dataf0 == 1){
                                $('#etapaCadastro').html('Remova o dedo do leitor.');
                                sleep(3000).then(() => {
                                    $('#etapaCadastro').html('Coloque novamente o mesmo dedo sobre o leitor');
                                    $.getJSON('/_read_finger1',{user:user,digital:digital},function(data) {
                                        if(data == -1){
                                            if(iotType == 1){
                                                $('#aviso3').html('Já existe um cadastro dessa digital!');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Já existe um cadastro dessa digital!');
                                            }
                                            document.getElementById('cadastramento').style.display = 'none';
                                            clearAvisos();
                                            user ='';
                                        }else if(data == 0){
                                            if(iotType == 1){
                                                $('#aviso3').html('Erro ao realizar cadastro');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Erro ao realizar cadastro');
                                            }
                                            document.getElementById('cadastramento').style.display = 'none';
                                            clearAvisos();
                                            user ='';
                                        }else if(data == 1){
                                            document.getElementById('cadastramento').style.display = 'none';
                                            if(iotType == 1){
                                                var quant = parseInt(digital) + 1;
                                                $('#aviso2').html('Digitais Cadastradas: '+quant+'/2.');
                                                $('#aviso3').html('Cadastro realizado com sucesso!');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Cadastro realizado com sucesso!');
                                            }
                                            clearAvisos();
                                            user ='';
                                        }else if(data == 2){
                                            if(iotType == 1){
                                                $('#aviso3').html('Erro ao cadastrar, amostras não são referentes ao mesmo dedo.');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Erro ao cadastrar, amostras não são referentes ao mesmo dedo.');
                                            }
                                            document.getElementById('cadastramento').style.display = 'none';
                                            clearAvisos();
                                            user ='';
                                        }else{
                                            if(iotType == 1){
                                                $('#aviso3').html('Erro ao realizar cadastro');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Erro ao realizar cadastro');
                                            }
                                            document.getElementById('cadastramento').style.display = 'none';
                                            clearAvisos();
                                            user ='';
                                        }
                                    });
                                });
                            }else if(dataf0 == -1){
                                user ='';
                                $('#aviso').html('Essa digital já foi cadastrada');
                                clearAvisos();
                                document.getElementById('cadastramento').style.display = 'none';
                            }else if(dataf0 == 2){
                                user ='';
                                $('#aviso').html('Cadastro cancelado');
                                clearAvisos();
                                document.getElementById('cadastramento').style.display = 'none';
                            }else{
                                user ='';
                                $('#aviso').html('Leitor biometrico não esta conectado');
                                clearAvisos();
                                document.getElementById('cadastramento').style.display = 'none';
                            }
                        });
                    }
                }else{
                    user ='';
                    document.getElementById('cadastramento').style.display = 'none';
                    $('#aviso').html('Leitor biometrico não esta conectado');
                    clearAvisos();
                }
             });
        }else if(data == 0){
            erroCadastro("Sem conexão, tente novamente mais tarde.");
        }else if(data == -1){
            erroCadastro("Nenhum usuario encontrado para o pin informado.");
        }else{
            erroCadastro("Pin incorreto!");
        }
    });
}

//----------------------------------------------- Usuario END -----------------------------------------------//

//----------------------------------------------- Funcionario STR -----------------------------------------------//

//Coloca o leitor biometrico no modo de cadastro
function CadastrarFuncionario(){
    $.getJSON('/cadastro',  function(data) {
        closeNav();
        modo = 4;
        $('#aviso').html('Informe o Código do Funcionario para Prosseguir.');
    });
}

//Valida se o pin informado é valido para o cadastro biometrico
function validarPinCadastroFuncionario(codigo){
    $.getJSON('/_validate_pin_cadastro_funcionario', {codigo:codigo} ,function(data) {
        if(data != 0 && data != -1 && data != null){
            var iotType = data['type'];
            user = data['cod'];
            $('#aviso').html("Iniciando cadastro para: <b> " + data['nome'] + "</b>");
             $.getJSON('/_count_digitais_funcionario', {user:user}, function(data) {
                if(data != -1){
                    digital = data;
                    $('#aviso2').append('<br>Digitais Cadastradas: '+data+'/2.');
                    if(data == 2){
                        if(iotType == 1){
                            $('#aviso3').html('Quantidade maxima de digitais cadastradas.');
                        }else if(iotType == 2){
                            $('#aviso').html('Quantidade maxima de digitais cadastradas.');
                        }
                        clearAvisos();
                        return;
                    }else{
                        $('#etapaCadastro').html('Coloque o dedo sobre o leitor');
                        setTimeout(function() {document.getElementById('cadastramento').style.display = 'block';}, 1000);
                        $.getJSON('/_read_finger0', function(dataf0) {
                            if(dataf0 == 1){
                                $('#etapaCadastro').html('Remova o dedo do leitor.');
                                sleep(3000).then(() => {
                                    $('#etapaCadastro').html('Coloque novamente o mesmo dedo sobre o leitor');
                                    $.getJSON('/_read_finger1_funcionario',{user:user,digital:digital},function(data) {
                                        if(data == -1){
                                            if(iotType == 1){
                                                $('#aviso3').html('Já existe um cadastro dessa digital!');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Já existe um cadastro dessa digital!');
                                            }
                                            document.getElementById('cadastramento').style.display = 'none';
                                            clearAvisos();
                                            user ='';
                                        }else if(data == 1){
                                            document.getElementById('cadastramento').style.display = 'none';
                                            if(iotType == 1){
                                                var quant = parseInt(digital) + 1;
                                                $('#aviso2').html('Digitais Cadastradas: '+quant+'/2.');
                                                $('#aviso3').html('Cadastro realizado com sucesso!');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Cadastro realizado com sucesso!');
                                            }
                                            clearAvisos();
                                            user ='';
                                        }else if(data == 2){
                                            if(iotType == 1){
                                                $('#aviso3').html('Erro ao cadastrar, amostras não são referentes ao mesmo dedo.');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Erro ao cadastrar, amostras não são referentes ao mesmo dedo.');
                                            }
                                            document.getElementById('cadastramento').style.display = 'none';
                                            clearAvisos();
                                            user ='';
                                        }else{
                                            if(iotType == 1){
                                                $('#aviso3').html('Erro ao realizar cadastro');
                                            }else if(iotType == 2){
                                                $('#aviso').html('Erro ao realizar cadastro');
                                            }
                                            document.getElementById('cadastramento').style.display = 'none';
                                            clearAvisos();
                                            user ='';
                                        }
                                    });
                                });
                            }else if(dataf0 == -1){
                                user ='';
                                $('#aviso').html('Essa digital já foi cadastrada');
                                clearAvisos();
                                document.getElementById('cadastramento').style.display = 'none';
                            }else{
                                user ='';
                                $('#aviso').html('Leitor biometrico não esta conectado');
                                clearAvisos();
                                document.getElementById('cadastramento').style.display = 'none';
                            }
                        });
                    }
                }else{
                    user ='';
                    document.getElementById('cadastramento').style.display = 'none';
                    $('#aviso').html('Leitor biometrico não esta conectado');
                    clearAvisos();
                }
              });
        }else if(data == 0){
            erroCadastro("Sem conexão, tente novamente mais tarde.");
        }else if(data == -1){
            erroCadastro("Nenhum funcionario encontrado para o código informado.");
        }else{
            erroCadastro("Codigo incorreto!");
        }
    });
}

//----------------------------------------------- Funcionario END -----------------------------------------------//

//================================================ Biometria END ================================================//


//================================================ Agendamento STR ================================================//

// Valida o pin e identifica o usuario para iniciar o processo de agendamento
function validatePinAgendamento(pin){
    $.getJSON('/_validate_pin_agendamento', {pin:pin} ,function(data) {
        if(data['result'] == 1){
            $('#userAgen').html(data['user']);
            user = data['cod']
            contratosUsuario(user);
            exibirModalAgendamento();
        }else{
            $('#aviso').html('Pin Incorreto!');
        }
        setTimeout(function() {$('#aviso').html('');}, 4000);
    });
}

// Valida pin para realizar o encerramento de um agendamento em andamento
function validarEncerramento(pin){
    $.getJSON('/_validate_pin', {pin:pin} ,function(data) {
        if(data['result'] == 2){
            document.getElementById('ence').style.visibility="hidden";
            $.getJSON('/_encerrar_reuniao',  {cod:data['cod']}, function(data) {
                $('#status').html('Sala Livre');
                $('#aviso').html("Uso encerrado com sucesso!");
            });
        }else{
            $('#aviso').html("Pin incorreto!");
        }
        setTimeout(function() {$('#aviso').html('');}, 4000);
    });
}

function contratosUsuario(user){
    $.getJSON('/_contratos', {usuario:user},function(data) {
        $('#contratos').empty();
        for(var i = 0; i < data.length; i++){
            var op = document.createElement("option");
            op.value = data[i]["cod"];
            op.text = data[i]["desc"];

            document.getElementById("contratos").add(op, null);
        }
    });
}

function iniciarAgendamento(){
    closeNav();
    $('#aviso').html('Informe seu pin para iniciar o agendamento.');
    modo = 3;
}

// Coloca o teclado em modo '-1' que espera receber o pin para o encerramento do agendamento
function encerrar(){
    $('#aviso').html('Informe seu pin para encerrar uso.');
    modo = -1;
}

// Atualiza UI informando que sala esta em uso
function iniciarUso(nome){
    $('#status').html('Em Reunião');
    $('#proximaReuniao').html('Reunião em Andamento: ' + nome);
    document.getElementById('ence').style.visibility="visible";
}

function agendar(){
    var dateParts = $('#datepicker').val().split("/");

    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    var dd = dateObject.getDate();

    var mm = dateObject.getMonth()+1;
    var yyyy = dateObject.getFullYear();
    if(dd<10)
    {
        dd='0'+dd;
    }

    if(mm<10)
    {
        mm='0'+mm;
    }

    inicio = yyyy+'-'+mm+'-'+dd + " " + $('#inicioAgen').val() + ":00";
    final = yyyy+'-'+mm+'-'+dd + " " +  $('#fimAgen').val()+":00";
    iot = document.getElementById("salas").value;
    descricao = '';
    usuario = user;
    contrato = document.getElementById("contratos").value;

    if(contrato != 0){
        $.getJSON('/_agendar', {inicio:inicio,final:final,iot:iot,descricao:descricao,usuario:usuario,contrato:contrato} ,function(data) {
           if(data == 0){
                $('#avisoAgendamento').html('Falha ao realizar agendamento.');
                setTimeout(function() {$('#avisoAgendamento').html('');}, 4000);
           }else if(data['success'] == 1){
                document.getElementById('modalAgendamento').style.display = 'none';
                $('#aviso').html(data['message']);
                setTimeout(function() {$('#aviso').html('');}, 4000);
                user = 0;
            }else{
                $('#avisoAgendamento').html(data['message']);
                setTimeout(function() {$('#avisoAgendamento').html('');}, 4000);
            }
        });
    }else{
        $('#avisoAgendamento').html('Selecione um contrato.');
        setTimeout(function() {$('#avisoAgendamento').html('');}, 4000);
    }
}

function exibirModalAgendamento(){
    document.getElementById('modalAgendamento').style.display = 'block';
}

function cancelaAgendamento(){
    document.getElementById('modalAgendamento').style.display = 'none';
}

//================================================ Agendamento END ================================================//


//================================================ Doc Ready STR ================================================//


//----------------------------------------------- Agendamento STR -----------------------------------------------//

// Preenche o select que permite a seleção da sala de reunião para o agendamento
function salasAgendamento(){
    $.getJSON('/_get_salas_reuniao',  function(data) {
        for(var i = 0; i < data.length; i++){
            var op = document.createElement("option");
            op.value = data[i]["iot"];
            op.text = data[i]["nome"];

            document.getElementById("salas").add(op, null);
        }
        document.querySelector('#salas').value = data[0]['iotAtual'];
    });
}

// Verifica qual o proximo agendamento para esse IOT
function verificarAgendamento(){
    $.getJSON('/_verificar_agendamento',  function(data) {
        $('#proximaReuniao').html('Próxima Reunião: ' + data['responsavel']);
        $('#inicio').html('Inicio: ' + data['inicio']);
        $('#fim').html('Fim: ' + data['fim']);
        if(data['andamento'] == 1){
            if(data['extrapolado'] == 0){
                $('#status').html('Em Reunião');
            }else{
                $('#status').html('Reunião Extrapolada');
            }
            document.getElementById('ence').style.visibility="visible";
        }
    });
}

//----------------------------------------------- Agendamento END -----------------------------------------------//

//----------------------------------------------- Biometria STR -----------------------------------------------//

//Verifica se uma tentativa de entrada por biometria foi realizada, exibindo o resultado na tela
function verificarEntradaBiometria(){
    $.getJSON('/_verificar_entrada_biometria',  function(data) {
        if(data['success'] == 1){
            var nomeU = data['nome'];
            if(data['permissao'] == 1){
                $('#aviso').html('Bem Vindo ' + nomeU +'!');
                $.getJSON('/Fechar', function(data) {
                     setTimeout(function() {$('#aviso').html('');}, 3000);
                });
            }else{
                $('#aviso').html(nomeU + ', você não possui acesso a esta unidade');
                setTimeout(function() {$('#aviso').html('');}, 4000);
            }
        }else if(data['success'] == 2){
            var nomeU = data['nome'];
            var acesso = data['permissao'];
            if(acesso == 0){
                  $('#aviso').html(nomeU + ', você não possui acesso a esta unidade');
                  setTimeout(function() {$('#aviso').html('');}, 4000);
            }else if(data['func'] == 1){
                $('#aviso').html('Bem Vindo ' + nomeU);
                $.getJSON('/Fechar', function(data) {
                     setTimeout(function() {$('#aviso').html('');}, 4000);
                });
            }else if(modo == -1){
                $.getJSON('/_encerrar_reuniao',  {cod:data['cod']}, function(data) {
                    $('#status').html('Sala Livre');
                    $('#aviso').html("Uso encerrado com sucesso!");
                    document.getElementById('ence').style.visibility="hidden";
                    setTimeout(function() {$('#aviso').html('');}, 3000);
                    resetarTeclado();
                });
            }else{
                $.getJSON('/_inciar_agendamento', {cod:data['cod']} ,function(data) {
                    $('#aviso').html('Bem Vindo ' + nomeU);
                    if(data['success'] == 1){
                        if(data['status'] == 0){
                            iniciarUso(nomeU);
                        }
                        $.getJSON('/Fechar', function(data) {
                             setTimeout(function() {$('#aviso').html('');}, 4000);
                        });
                    }else{
                        setTimeout(function() {$('#aviso').html('Aguarde até pelo menos 5 minutos antes de seu agendamento para iniciar');}, 4000);
                        setTimeout(function() {$('#aviso').html('');}, 8000);
                    }

                });
            }
        }else if(data['success'] == 3){
            $('#aviso').html(data['nome']);
            $('#aviso2').html(data['msg']);
            if(data['permissao'] == 1){
                $.getJSON('/Fechar', function(data) {
                     setTimeout(function() {$('#aviso').html('');}, 3000);
                     setTimeout(function() {$('#aviso2').html('');}, 3000);
                });
            }
            setTimeout(function() {$('#aviso').html('');}, 3000);
            setTimeout(function() {$('#aviso2').html('');}, 3000);
        }else if(data == -1){
            $('#aviso').html('Digital invalida!');
            setTimeout(function() {$('#aviso').html('');}, 3000);
        }
    });
}

//----------------------------------------------- Biometria END -----------------------------------------------//

//----------------------------------------------- UI Update STR -----------------------------------------------//

// Verifica a data e hora atual, atualizando a UI com o resultado
function relogio(){
    var dia = new Date();
    var h = dia.getHours();
    var m = dia.getMinutes();

    var dd = dia.getDate();

    var mm = dia.getMonth()+1;
    var yyyy = dia.getFullYear();

    if(dd<10) {
        dd = '0'+dd;
    }

    if(mm<10) {
        mm = '0'+mm;
    }

    today = dd + '/' + mm + '/' + yyyy;

    h = checkTime(h);
    m = checkTime(m);

    document.getElementById('hora').innerHTML = h + ":" + m;
    document.getElementById('data').innerHTML = today;
}

// Ajusta a formatação da data/hora
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

// Verifica se o dispositivo esta conectado e atualiza a UI com o resultado
function checkConnection(){
    $.getJSON('/_get_conexao',  function(data) {
        document.getElementById('con').innerHTML = data.toString();
    });
}

//----------------------------------------------- UI Update END -----------------------------------------------//

$(document).ready(function() {
    var modal = document.getElementById('myModal');

    var btn = document.getElementById("myBtn");

    var span = document.getElementById("close");

    btn.onclick = function() {
        closeNav();
        modo = 2;
        $('#aviso').html('Informe o PIN do administrador para acessar o painel de configurações.');
    }

    span.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    salasAgendamento();
    setInterval(verificarEntradaBiometria, 2000);
    setInterval(relogio, 1000);
    setInterval(checkConnection, 30000);
    setInterval(verificarAgendamento, 10000);
});

//======================== Doc Ready END ========================//
