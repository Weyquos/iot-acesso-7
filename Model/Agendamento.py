class Agendamento:

    def __init__(self, idagendamento, cliente, inicio, fim, iotid, iniciado, encerrado, andamento, contrato):
        self.id = idagendamento
        self.responsavel = cliente
        self.inicio = inicio
        self.fim = fim
        self.iotid = iotid
        self.iniciado = iniciado
        self.encerrado = encerrado
        self.andamento = andamento
        self.contrato = contrato

