import datetime
import json
import sqlite3
import threading
import urllib
import os

#import RPi.GPIO as GPIO
import pymysql as pymysql
import pymysql.cursors
import requests
from flask import Flask, request, render_template, jsonify, json
from gpiozero import LED
import time

from subprocess import check_output

from dateutil.relativedelta import *

from Model.Agendamento import Agendamento
from Model.Usuario import Usuario
from pyfingerprint import PyFingerprint

app = Flask(__name__)

leitorStatus = False  # Indica se o dispositivo esta conectado com o leitor biometrico

f = None  # Inicializa variavel para o objeto do leitor

iotID = 1565  # IOT ID(Define a sala que dispositivo esta gerenciando)

doorTime = 5  # Tempo, em segundos, que a porta se mantem aberta após liberação

updateHora = 2  # Hora do update automatico

updateMin = 10  # Min do update automatico

doorStatus = False  # Indica se a porta deve estar fechada(FALSE) ou aberta (TRUE)

guestStatus = False

update = False  # Quando True inicia update das digitais cadastradas no leitor local

updateStatus = False

bioCancelada = False

modo = 'Leitura'  # Modo do leitor biometrico(Leitura / Cadastro)

listaPermissoes = []  # Usuario com permissão de acesso na sala

listaPermissoesFuncionario = []  # Funcionarios com permissão de acesso a sala

digital = 0  # Leitura que esta sendo cadastrada na biometria(primeira ou segunda imagem que sera convertida para o template)

ip = check_output(['hostname', '-I'])  # Verifica ip do dispositivo
ip = str(ip).strip('b').strip("'")
ip = ip[:-3]

resultadoBio = 0  # Registra tentativa de acesso via biometria

iotType = 0  # Tipo de iot (-1 = Falha ao identificar / 0 = Sala Comum / 1 = Sala Dedicada / 2 = Sala de Reunião)

proximoAgendamento = Agendamento(-1, 'Sem Agendamentos', ' -- ', ' -- ', iotID, 0, 1, 0, 0)

Admin = Usuario(5, 'Admin', '', '--')

sqlitePath = --

reader = sqlite3.connect(sqlitePath, isolation_level=None)
reader.execute('pragma journal_mode=wal;')
reader.close()

# ==================================Produção=================================#
iotUrl = --
ipbanco = --
userbanco = --
passbanco = --
db = --
charset = 'utf8mb4'
# ===========================================================================#

posicao = -1

'''
Verifica se o dispositivo possui configuracoes locais anteriotes. Caso existam, esses valores sao utilizados.
iotID       -> Define qual o dispositivo atual. 
doorTime    -> Defiene o tempo de abertura da porta.
updateHora  -> Hora programada para update automatico.      ==REMOVER==
updateMin   -> Minuto programado para update automatico.    ==REMOVER==
ipbanco     -> Define IP de acesso ao banco de dados.       ==REMOVER==
userbanco   -> Define usuario de acesso ao banco de dados.  ==REMOVER==
passbanco   -> Define senha de acesso ao banco de dados.    ==REMOVER==
db          -> Define banco de dados a ser utilizado.       ==REMOVER==
charset     -> Define charset a ser utilizado no banco.     ==REMOVER==
'''

# Registra erros ocorridos durante a execucao do programas
def registrarLogErro(e):
    try:
        conn = sqlite3.connect(sqlitePath)

        cursor = conn.cursor()

        sql = "INSERT INTO log(erro, hora) VALUES (?,?)"
        cursor.execute(sql, (e, datetime.datetime.now()))
        conn.commit()

        conn.close()
    except Exception as e:
        print(e)

def verificar_configuracoes_locais():
    try:
        global iotID
        global doorTime
        global updateHora
        global updateMin
        global ipbanco
        global userbanco
        global passbanco
        global db
        global charset

        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("SELECT * FROM configuracao;")
        row = cursorLocal.fetchone()
        if row is None:
            cursorLocal.execute(
                "INSERT INTO configuracao VALUES(:iot,:abertura,:updateH,:updateM,:ip,:user,:pass,:database,:charset);",
                {'iot': iotID, 'abertura': doorTime, 'updateH': updateHora, 'updateM': updateMin, 'ip': ipbanco,
                 'user': userbanco, 'pass': passbanco, 'database': db, 'charset': charset})
            conn.commit()
        else:
            iotID = int(row[0])
            doorTime = int(row[1])
            updateHora = int(row[2])
            updateMin = int(row[3])
            ipbanco = row[4]
            userbanco = row[5]
            passbanco = row[6]
            db = row[7]
            charset = row[8]
        conn.close()
    except Exception as e:
        registrarLogErro(e)


verificar_configuracoes_locais()

'''
Realiza conexão com leitor biometrico
Erro -> leitorStatus setado para false, impedindo que operações que necessitam dele sejam executadas


def conectarLeitor():
    global leitorStatus
    global f
    try:
        f = PyFingerprint('/dev/ttyS0', 57600, 0xFFFFFFFF, 0x00000000)
        leitorStatus = True
    except Exception as e:
        registrarLogErro(str(e))
        leitorStatus = False
'''

'''
Thread de controle do update automatico da lista de permissoes
Processo ocorre em intervalos definidos por Interval em seu construtor
'''
class AccessList(object):

    def __init__(self, interval=3600):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        global iotID
        global modo
        with app.app_context():
            try:
                while True:
                    time.sleep(self.interval)
                    if iotType == 1:
                        if iotID != 1565 and modo != 'Cadastro':
                            carregaUser()
                        elif modo != 'Cadastro':
                            carregarUserSalaComum()
            except Exception as e:
                registrarLogErro(str(e))
                self.run()

accessUpdate = AccessList()  # Inicia thread de atualização da lista de permissoes

'''
Thread para controlar a abertura da porta
doorStatus:True -> Aciona o rele pelo tempo definido em doorTime
'''
class DoorControl(object):

    def __init__(self, interval=1):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        try:
            global doorStatus
            # global guestStatus
            while True:
                if doorStatus:
                    led = LED(17)
                    time.sleep(doorTime)
                    led.close()
                    doorStatus = False
                    # guestStatus = False
                time.sleep(self.interval)
        except Exception as e:
            registrarLogErro(str(e))
            self.run()

doorControler = DoorControl()  # Inicia Thread de Controle da Porta

'''
Thread para controlar o aviso de visitante
guestStatus:True -> Aciona o rele da placa de visitante
'''
class guestControl(object):

    def __init__(self, interval=1):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    def run(self):
        try:
            global guestStatus
            ledGuest = LED(22)
            while True:
                if guestStatus:
                    ledGuest.close()
                    while guestStatus:
                        time.sleep(5)
                    ledGuest = LED(22)
                time.sleep(self.interval)
        except Exception as e:
            registrarLogErro(str(e))
            self.run()


# guestControler = guestControl()   # Inicia Thread de Controle da Porta

'''
Thread que gerencia o leitor biometrico
Modo:Leitura    -> Leitor aguardando leitura de digital para verificar permissão
Modo:Cadastro   -> Leitor aguardando leitura de duoas digitais para gerar template
                Digital:0   -> Aguardando primeira Digital
                Digital:1   -> Aguardando segunda Digital
Update:True     -> Inicia processo de update das digitais armazenadas no leitor



class leitor(object):

    def __init__(self, interval=1):
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True  # Daemonize thread
        thread.start()  # Start the execution

    def run(self):
        global modo
        global digital
        global leitorStatus
        global iotType
        global update
        global updateStatus
        try:
            conectarLeitor()  # Inicia o leitor biometrico
            while True:
                if leitorStatus:
                    while not f.readImage() and modo == 'Leitura':
                        if update:
                            if iotType == 1:
                                if iotID == 1565:
                                    carregarUserSalaComum()
                                else:
                                    carregaUser()
                            elif iotType == 2:
                                update = False
                                updateDigitais()
                                updateStatus = False

                        pass
                    if modo == 'Leitura':
                        f.convertImage(0x01)
                        result = f.searchTemplate()
                        positionNumber = result[0]
                        identificarLeitura(positionNumber)

                    while not f.readImage() and modo == 'Cadastro':
                        pass
                    if digital == 0 and modo == 'Cadastro':
                        f.convertImage(0x01)
                        digital = 1
                    if digital == 1 and modo == 'Cadastro':
                        f.convertImage(0x02)
                        digital = 0
                        modo = 'Leitura'
                else:
                    conectarLeitor()
                time.sleep(self.interval)
        except Exception as e:
            registrarLogErro(str(e))
            leitorStatus = False
            self.run()


leitorDigital = leitor()  # Inicializa Thread de controle do leitor
'''

'''
Verifica o nome da empresa de acordo com o iot atual
Atualiza o registro local
Retorno -> Nome da empresa
Erro -> Uso o ultimo nome armazenado no dispositivo
'''
def get_IOT_nome():
    global iotID
    try:
        iot = requests.get(iotUrl + "?classe=IOT&metodo=CheckIOT&atributo[iotid]=" + str(iotID))
        empresa = json.loads(iot.text)['list'][0]['descricaoservico']

        conn = sqlite3.connect(sqlitePath)

        cursorLocal = conn.cursor()
        cursorLocal.execute("DELETE FROM empresa;")
        conn.commit()
        cursorLocal.execute("INSERT OR REPLACE INTO empresa VALUES(0,?);", (empresa,))
        conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))
        empresa = ''
        conn = sqlite3.connect("/home/pi/iotyoudo.db")
        cursorLocal = conn.cursor()
        cursorLocal.execute("SELECT nome FROM empresa;")
        for row in cursorLocal.fetchall():
            empresa = row[0]
        conn.close()
    return empresa


'''
Carrega permissões de acesso para usuarios em uma sala dedicada
Inicializa update do leitor biometrico caso exista conexão e o leitor esteja conectado
Retorno -> Nome da empresa da sala
Erro -> Carrega as permissões de acesso da ultima execução bem sucedida
'''
def carregaUser():
    global leitorStatus
    global listaPermissoes
    global updateStatus
    global update
    empresa = get_IOT_nome()
    update = False
    try:
        permissoes = requests.get(
            iotUrl + "?classe=IOT&metodo=ListarUsuariosAutorizadosPorIdAgendaServico&atributo[iotid]=" + str(iotID))
        listaPermissoes.clear()
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        for user in json.loads(permissoes.text)['list']:
            cod = user['idusuario']
            cursorLocal.execute("SELECT user.id, user.nome, user.pin, '--' FROM usuario user WHERE user.id = ?;",
                                (cod,))
            for row in cursorLocal.fetchall():
                usuario = Usuario(row[1], row[0], row[2], row[3])
                listaPermissoes.append(usuario)
        conn.close()
        get_admin()
        atualizar_permissoes_local()
        updateFuncionariosPermissoes()
        if leitorStatus:
            updateDigitais()
    except Exception as e:
        registrarLogErro(str(e))
        carregar_permissoes_local()
    updateStatus = False
    return empresa


'''
Carrega para a lista de permissoes todos os usuarios vinculados com um contrato Vigente
Inicializa update do leitor biometrico caso exista conexão e o leitor esteja conectado
Retorno -> Nome da empresa da sala
Erro -> Carrega as permissões de acesso da ultima execução bem sucedida
'''
def carregarUserSalaComum():
    global update
    global leitorStatus
    global updateStatus
    global listaPermissoes
    update = False
    empresa = get_IOT_nome()
    cont = 0
    try:
        listaPermissoes.clear()
        usuarios = requests.get("http://alugueap.com/userpweb/api/rasp/get-usuarios-iot.php")
        for user in json.loads(usuarios.text):
            usuario = Usuario(user["Nome"], user["Cod"], user["Pin"], '--')
            listaPermissoes.append(usuario)
        get_admin()
        get_Comercial()
        atualizar_permissoes_local()
        updateFuncionariosPermissoes()
        if leitorStatus:
            updateDigitais()
    except Exception as e:
        carregar_permissoes_local()
        registrarLogErro("Carregar Users Sala Comum: " + str(cont) + " -  " + str(e))
    updateStatus = False
    return empresa


'''
Renderiza a tela inicial da aplicação
iotType:0 -> Sala Comum.
iotType:1 -> Sala Dedicada.
iotType:2 -> Sala de Reunião.
'''
@app.route('/')
def index():
    global ip
    global proximoAgendamento
    checkIotType()
    if iotType == 0:
        # TODO Carregar usuarios de Sala Comum
        return jsonify(0)
    elif iotType == 1:
        if iotID == 1565:
            empresa = carregarUserSalaComum()
        else:
            empresa = carregaUser()
        return render_template('sala_dedicada.html', status="Sala Dedicada", user=empresa,
                               data=time.strftime("%d/%m/%Y"), hora=time.strftime("%H:%M"), conexao="Online",
                               versao="1.0.0", device="YouDO-" + str(iotID), ip=ip)
    elif iotType == 2:
        try:
            iot = requests.get(iotUrl + "?classe=IOT&metodo=CheckIOT&atributo[iotid]=" + str(iotID))
            empresa = json.loads(iot.text)['list'][0]['descricaoservico']
        except Exception as e:
            empresa = "Meeting Room"
            print(e)
        get_admin()
        get_agendamento_dia()
        updateFuncionariosPermissoes()
        return render_template('sala_reuniao.html', status="Sala Livre", user=empresa, data=time.strftime("%d/%m/%Y"),
                               hora=time.strftime("%H:%M"), inicio=proximoAgendamento.inicio.split(' ', 1)[1][:-3],
                               fim=proximoAgendamento.fim.split(' ', 1)[1][:-3],
                               responsavel=proximoAgendamento.responsavel, conexao="Online", versao="1.0.0",
                               device="YouDO-" + str(iotID), ip=ip)
    else:
        return jsonify(0)


'''
Verifica se o pin informado tem permissão para acessar a sala. 
Result:0 -> Tentativa de entrada, PIN invalido.
Result:1 -> Entrada em sala dedicada.
Result:2 -> Entrada em sala de Reunião.
Result:3 -> Usuario admin, libera a porta.
'''
@app.route('/_validate_pin')
def validate_pin():
    global listaPermissoes
    global iotID
    try:
        pin = request.args.get('pin')
        if pin == Admin.pin:
            resposta = {'nome': 'Admin', 'result': 3, 'cod': 0}
            return jsonify(resposta)
        for user in listaPermissoes:
            if user.pin == pin:
                tipo = getTipoUsuario(user.cod)
                valor, statusC, desconto, statusD = VerificarCOnfiguracoesSigv()
                if tipo == 'Y' and int(statusC) == 1:
                    if iotID == 1565:
                        '''
                        cred = VerificarCreditos(int(user.cod), valor)
                        if cred == 0:
                            resposta = {'nome': user.nome, 'result': 4, 'permissao': 0,
                                        'msg': 'Você não possui créditos para acessar essa unidade.'}
                        elif cred == 1:
                            resposta = {'nome': user.nome, 'result': 4, 'permissao': 1,
                                        'msg': 'R$' + str(valor) + ' foram utilizados.'}
                        else:
                        '''
                        resposta = {'nome': user.nome, 'result': 4, 'permissao': 1, 'msg': 'Bem Vindo !'}
                    else:
                        resposta = {'result': 1, 'nome': user.nome, 'permissao': 0}
                    return jsonify(resposta)
                else:
                    if iotType == 2:
                        resposta = {'nome': user.nome, 'result': 2, 'cod': user.cod}
                        return jsonify(resposta)
                    else:
                        resposta = {'nome': user.nome, 'result': 1}
                        return jsonify(resposta)
    except Exception as e:
        registrarLogErro(e)
    resposta = {'nome': '--', 'result': 0}
    return jsonify(resposta)


'''
Valida pin para exibição da tela de configurações
Return:1 -> Pin de administrador correto.
Return:0 -> Pin incorreto.
'''
@app.route('/_validate_pin_admin')
def validade_pin_admin():
    pin = request.args.get("pin")
    if pin == Admin.pin:
        return jsonify(1)
    else:
        return jsonify(0)


'''
Libera a porta pelo tempo definido em doorTime
Ao setar doorStatus = True a Thread de controle da porte realiza o processo
'''
@app.route('/Fechar')
def abrir_fechar():
    global doorStatus
    doorStatus = True
    return jsonify(1)


'''
Corta energa do rele, ativando a luz do sinal de aviso de visitante
guestStatus:True -> Libera que a Thread de controle realize o processo
'''
@app.route('/_campainha')
def campainha():
    global guestStatus
    guestStatus = True
    return jsonify(0)


'''
Libera Thread de controle do leitor para iniciar update
Return:0 -> Leitor não conectado
Return:1 -> Leitor conectado, libera update.
'''

@app.route('/_update')
def updateAcessos():
    global update
    global updateStatus

    update = True
    updateStatus = True
    time.sleep(3)
    #while updateStatus:

    if iotID == 1565:
        empresa = carregarUserSalaComum()
    else:
        empresa = carregaUser()

    return jsonify(1)



'''
@app.route('/_update')
def updateAcessos():
    global update
    global updateStatus
    if not leitorStatus:
        return jsonify(0)
    update = True
    updateStatus = True
    time.sleep(3)
    while updateStatus:
        if not leitorStatus:
            updateStatus = False
            return jsonify(0)
        pass
    return jsonify(1)

'''

'''
Verifica se é possivel se copnectar com a API
Caso a conexão seja bem sucedida os dados de inicio e fim de reunião em periodo offline são atualizados
'''
@app.route('/_get_conexao')
def get_conexao():
    try:
        urllib.request.urlopen("http://api.youdobrasil.com.br", timeout=3)
        con = "Online"
        # atualizar_prod_registros_off()
    except Exception as e:
        print(e)
        con = "Offline"
    return jsonify(con)


'''
Coloca a Thread de controle do leitor em modo de cadastro
'''
@app.route('/cadastro')
def rend_cadastro():
    global modo
    global updateStatus
    if updateStatus:
        return jsonify(0)
    modo = 'Cadastro'
    return jsonify(1)


'''
Coloca a Thread de controle do leitor em modo de leitura
'''
@app.route('/leitura')
def leitura():
    global modo
    modo = 'Leitura'
    return jsonify(1)


'''
Reinicia o dispositivo
'''
@app.route('/reboot')
def reboot():
    os.system('sudo reboot')
    return jsonify(1)


'''
Altera valor de doorTime
doorTime -> Define o tempo que o rele fica acionado para abrir a porta
'''
@app.route('/changeDoorTime')
def changeDoorTime():
    global doorTime
    global iotID
    openTime = request.args.get('time')
    doorTime = int(openTime)
    conn = sqlite3.connect(sqlitePath)
    cursor = conn.cursor()
    sql = "UPDATE configuracao SET tempoAbertura = ? WHERE iotID = ?;"
    cursor.execute(sql, (doorTime, iotID))
    conn.commit()
    conn.close()
    return jsonify(1)


'''
Altera valor de iotID
doorTime -> Define o dispositivo que o iot esta gerenciando.
'''
@app.route('/changeIOT')
def chengeIOT():
    global iotID
    idNovoIOT = request.args.get('iot')
    iotID = idNovoIOT
    conn = sqlite3.connect(sqlitePath)
    cursor = conn.cursor()
    sql = "UPDATE configuracao SET iotID = ?;"
    cursor.execute(sql, (iotID,))
    conn.commit()
    conn.close()
    return jsonify(1)


@app.route('/cancelar_bio')
def cancelarBio():
    global bioCancelada
    bioCancelada = True
    return jsonify(0)


@app.route('/att_app')
def attApp():
    path = "/home/pi"
    clone = "git clone https://gitlab-ci-token:x7QJwEywsz34JmKsGD-P@gitlab.com/desenvolvimentoyoudo/controle-acesso" \
            "-7.git "

    os.system("sudo rm -rf /home/pi/controle-acesso-7")
    os.chdir(path)
    os.system(clone)
    os.system('sudo reboot')

    return jsonify(1)

@app.route('/del_banco')
def delBanco():

    os.system("sudo rm -rf /home/pi/iotyoudo.db")
    os.system('sudo reboot')

    return jsonify(1)


'''
Atualiza as digitais do leitor de acordo com a lista de permissoes.
Relaciona a posição em que a digital foi inserida no leitor com o usuario a qual pertence
'''
def updateDigitais():
    global posicao
    cont = 0
    try:
        f.clearDatabase()
        resetPosicoesBio()
        digitais = requests.get("http://alugueap.com/userpweb/api/rasp/get-digitais-usuarios.php")
        for user in json.loads(digitais.text):
            idUsuario = int(user['Cod'])
            conn = sqlite3.connect(sqlitePath)
            cursorLocal = conn.cursor()
            cursorLocal.execute("INSERT OR IGNORE INTO usuarioPosicao VALUES(?,?,?)", (idUsuario, None, None))
            conn.commit()
            if user['d0'] is not None:
                f.uploadCharacteristics(0x02, eval(user['d0']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update usuarioPosicao SET pd0 = ? WHERE idUsuario = ?;", (posicao, idUsuario))
                conn.commit()
                cont = cont + 1
            if user['d1'] is not None:
                f.uploadCharacteristics(0x02, eval(user['d1']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update usuarioPosicao SET pd1 = ? WHERE idUsuario = ?;", (posicao, idUsuario))
                conn.commit()
                cont = cont + 1
            conn.close()
    except Exception as e:
        registrarLogErro(e)
    updateDigitaisFuncionarios(cont)


'''
Atualiza a lista de funcinarios que possuem permissão a unidade.
'''
def updateFuncionariosPermissoes():
    global iotID
    global listaPermissoesFuncionario
    try:
        acessos = requests.get(
            "http://alugueap.com/userpweb/api/rasp/get-acessos-funcionarios-iot.php?iot=" + str(iotID))
        listaPermissoesFuncionario = []
        for user in json.loads(acessos.text):
            codFuncionario = int(user['Cod'])
            listaPermissoesFuncionario.append(codFuncionario)
        atualizarPermissoesFuncionarioLocal()
    except Exception as e:
        registrarLogErro(str(e))
        carregarPermissoesFuncionarioLocal()


'''
Atualiza a base local com a lista de funcionarios com acesso à unidade.
'''
def atualizarPermissoesFuncionarioLocal():
    try:
        global listaPermissoesFuncionario
        ids = []
        for func in listaPermissoesFuncionario:
            acesso = {'id': 0, 'cod': func}
            ids.append(acesso)
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("DELETE FROM acessoFuncionario;")
        conn.commit()
        cursorLocal.executemany("INSERT OR REPLACE INTO acessoFuncionario VALUES(:id,:cod);", ids)
        conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(e)


'''
Carraga as permissoes de funcionarios com base no armazenamento local(ultima atualização efetivada)
'''
def carregarPermissoesFuncionarioLocal():
    try:
        global listaPermissoesFuncionario
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("SELECT funcionario FROM acessoFuncionario;")
        for row in cursorLocal.fetchall():
            listaPermissoesFuncionario.append(row[0])
        conn.close()
    except Exception as e:
        registrarLogErro(e)


'''
Atualiza as digitais do leitor de acordo com a lista de permissoes.
Relaciona a posição em que a digital foi inserida no leitor com o funcionario a qual pertence
'''
def updateDigitaisFuncionarios(cont):
    global posicao
    updateFuncionariosPermissoes()
    try:
        digitais = requests.get('http://alugueap.com/userpweb/api/rasp/get-digitais-funcionarios.php')
        for user in json.loads(digitais.text):
            codFuncionario = int(user['Cod'])
            conn = sqlite3.connect(sqlitePath)
            cursorLocal = conn.cursor()
            cursorLocal.execute("INSERT OR IGNORE INTO funcionarioPosicao VALUES(?,?,?)", (codFuncionario, None, None))
            conn.commit()
            if user['d0'] is not None:
                f.uploadCharacteristics(0x02, eval(user['d0']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update funcionarioPosicao SET pd0 = ? WHERE idFuncionario = ?;",
                                    (posicao, codFuncionario))
                conn.commit()
                cont = cont + 1
            if user['d1'] is not None:
                f.uploadCharacteristics(0x02, eval(user['d1']))
                posicao = f.storeTemplate(cont, 0x02)
                cursorLocal.execute("Update funcionarioPosicao SET pd1 = ? WHERE idFuncionario = ?;",
                                    (posicao, codFuncionario))
                conn.commit()
                cont = cont + 1
            conn.close()
    except Exception as e:
        registrarLogErro(str(e))


'''
Valida se o pin informado é valido para iniciar o cadastro de digital do usuario
Return:Usuario  ->Nome: nome do usuario
                ->Cod:  id do usuario
Return:-1       ->Pin não é valido
'''
@app.route('/_validate_pin_cadastro')
def get_user():
    global iotType
    nome = ''
    try:
        urllib.request.urlopen("http://api.youdobrasil.com.br", timeout=3)

        pin = request.args.get('pin')
        iot = requests.get(iotUrl + "?classe=IOT&metodo=ConsultarPinUsuarioByPin&atributo[pin]=" + pin)
        cod = json.loads(iot.text)['list']['idusuario']
        if cod is None or cod == 'null':
            return jsonify(-1)

        conn = sqlite3.connect(sqlitePath)

        cursor = conn.cursor()
        cursor.execute("SELECT user.nome FROM usuario user WHERE user.id = ?;", (cod,))
        for row in cursor.fetchall():
            nome = row[0]
        usuario = {'nome': nome, 'cod': cod, 'type': iotType}
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)
    return jsonify(usuario)


'''
Valida se o código informado é valido para iniciar o cadastro de digital do funcionario
Return:Usuario  ->  Nome: nome do funcionario
                ->  Cod:  codigo do funcionario
Return:0        ->  Erro
'''
@app.route('/_validate_pin_cadastro_funcionario')
def get_funcionario():
    global iotType
    nome = ''
    codigo = request.args.get('codigo')
    try:
        urllib.request.urlopen("http://api.youdobrasil.com.br", timeout=3)

        conn = sqlite3.connect(sqlitePath)

        cursor = conn.cursor()
        cursor.execute("SELECT func.nome FROM funcionario func WHERE id = ?;", (codigo,))
        for row in cursor.fetchall():
            nome = row[0]
        usuario = {'nome': nome, 'cod': codigo, 'type': iotType}
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)
    return jsonify(usuario)


'''
Aguarda a primeira leitura biometrica para realizacao do cadastro
Return:0 -> Falha de conexão com o leitor biometrico
Return:1 -> Processi realizado com sucesso
'''
@app.route('/_read_finger0')
def get_finger0():
    global digital
    global leitorStatus
    global modo
    global bioCancelada
    digital = 0

    if leitorStatus:
        while digital == 0:
            if not leitorStatus:
                return jsonify(0)
            if bioCancelada:
                bioCancelada = False
                modo = 'Leitura'
                return jsonify(2)
            pass
    else:
        return jsonify(0)
    return jsonify(1)


'''
Cria o template da digital para o cadstro, armazenando localmente e no banco
Return:-1 -> Digital já esta cadastrada
Return:0 -> Falha de conexão com o leitor biometrico
Return:1 -> Processi realizado com sucesso
Return:2 -> As amostrar para a criação do template não são referentes ao mesmo dedo
'''
@app.route('/_read_finger1')
def get_finger1():
    global digital
    global leitorStatus
    cnx = None
    try:
        user = request.args.get('user')
        digi = request.args.get('digital')
        while digital == 1:
            if not leitorStatus:
                return jsonify(0)
            pass

        if f.compareCharacteristics() == 0:
            return jsonify(2)

        f.createTemplate()
        characteristics = str(f.downloadCharacteristics(0x01)).encode('utf-8')

        result = f.searchTemplate()
        positionNumber = result[0]

        if positionNumber >= 0:
            return jsonify(-1)
        # resultCadastro = requests.get("http://alugueap.com/userpweb/api/rasp/cadastrar-digital-usuario.php?user="+str(user)+"&nDigital="+str(digi)+"&digital="+str(characteristics)).text
        idLast = get_last_digital_id()
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)
        with cnx.cursor() as cursor:
            query = "INSERT INTO tab_ydo_digital_usuario(iddigitalusuario,idusuario,numerodigital,digital) VALUES(%s,%s,%s,%s)"
            args = (idLast, user, digi, characteristics)
            cursor.execute(query, args)
        cnx.commit()
        # registrarLogErro(resultCadastro)
        # if int(resultCadastro) == 0:
        #     return jsonify(0)

        position = f.storeTemplate()

        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("INSERT OR IGNORE INTO usuarioPosicao VALUES(?,?,?)", (user, None, None))
        conn.commit()

        if int(digi) == 0:
            cursorLocal.execute("Update usuarioPosicao SET pd0 = ? WHERE idUsuario = ?;", (position, user))
            conn.commit()
        if int(digi) == 1:
            cursorLocal.execute("Update usuarioPosicao SET pd1 = ? WHERE idUsuario = ?;", (position, user))
            conn.commit()
        conn.close()

    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)
    finally:
        if cnx is not None:
            cnx.close()
    return jsonify(1)


'''
Cria o template da digital para o cadstro, armazenando localmente e no banco
Return:-1 -> Digital já esta cadastrada
Return:0 -> Falha de conexão com o leitor biometrico
Return:1 -> Processo realizado com sucesso
Return:2 -> As amostrar para a criação do template não são referentes ao mesmo dedo
'''
@app.route('/_read_finger1_funcionario')
def get_finger1_funcionario():
    global digital
    global leitorStatus
    cnx = None
    try:
        user = request.args.get('user')
        digi = request.args.get('digital')
        while digital == 1:
            if not leitorStatus:
                return jsonify(0)
            pass
        if f.compareCharacteristics() == 0:
            return jsonify(2)
        f.createTemplate()

        characteristics = str(f.downloadCharacteristics(0x01)).encode('utf-8')
        idLast = get_last_digital_funcionario_id()

        result = f.searchTemplate()
        positionNumber = result[0]

        if positionNumber >= 0:
            return jsonify(-1)

        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)
        with cnx.cursor() as cursor:
            query = "INSERT INTO tab_ydo_digital_funcionario(iddigitalfuncionario,idfuncionario,numerodigital,digital) VALUES(%s,%s,%s,%s)"
            args = (idLast, user, digi, characteristics)
            cursor.execute(query, args)
        cnx.commit()

        position = f.storeTemplate()

        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("INSERT OR IGNORE INTO funcionarioPosicao VALUES(?,?,?)", (user, None, None))
        conn.commit()
        if int(digi) == 0:
            cursorLocal.execute("Update funcionarioPosicao SET pd0 = ? WHERE idFuncionario = ?;", (position, user))
            conn.commit()
        if int(digi) == 1:
            cursorLocal.execute("Update funcionarioPosicao SET pd1 = ? WHERE idFuncionario = ?;", (position, user))
            conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)
    finally:
        if cnx is not None:
            cnx.close()
    return jsonify(1)


'''
Retorna o ultimo id registrado na tabela de digitais de usuarios
'''
def get_last_digital_id():
    idLast = 0
    cnx = None
    cursor = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute("SELECT coalesce(MAX(iddigitalusuario), 0) as id FROM tab_ydo_digital_usuario")

        result_set = cursor.fetchall()
        for row in result_set:
            idLast = row["id"]
    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return idLast + 1


'''
Retorna o ultimo id registrado na tabela de digitais de funcionarios
'''
def get_last_digital_funcionario_id():
    idLast = 0
    cnx = None
    cursor = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute("SELECT coalesce(MAX(iddigitalfuncionario), 0) as id FROM tab_ydo_digital_funcionario")

        result_set = cursor.fetchall()
        for row in result_set:
            idLast = row["id"]
    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return idLast + 1


'''
Retorna a quantidade de digitais cadastradas para o usuario. A quantidade maxima permitida é 2 cadastros.
'''
@app.route('/_count_digitais')
def get_count_digitais_usuario():
    global leitorStatus
    if not leitorStatus:
        return jsonify(-1)
    cursor = None
    cnx = None
    user = request.args.get('user')
    cont = 0
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute("select COUNT(idusuario) as digitais from tab_ydo_digital_usuario where idusuario = %s", [user])

        result_set = cursor.fetchall()
        for row in result_set:
            cont = row["digitais"]
    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()

    return jsonify(cont)


'''
Retorna a quantidade de digitais cadastradas para o funcionario. A quantidade maxima permitida é 2 cadastros.
'''
@app.route('/_count_digitais_funcionario')
def get_count_digitais_funcionario():
    global leitorStatus
    if not leitorStatus:
        return jsonify(-1)
    cursor = None
    cnx = None
    user = request.args.get('user')
    cont = 0
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "select COUNT(idfuncionario) as digitais from tab_ydo_digital_funcionario where idfuncionario = %s", [user])

        result_set = cursor.fetchall()
        for row in result_set:
            cont = row["digitais"]
    except Exception as e:
        registrarLogErro(e)
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()

    return jsonify(cont)


'''
Identifica a posição da digital reconhecida.
resultadoBio: -1 -> Digital não foi reconhecida.
resultadoBio:  1 -> Digital reconhecida, sala dedicada.
resultadoBio:  2 -> Digital reconhecida, sala de reunião.
'''
def identificarLeitura(resultPosition):
    global posicao
    global resultadoBio
    global iotType

    if resultPosition == -1:
        resultadoBio = -1
    elif iotType == 1:
        resultadoBio = 1
        posicao = resultPosition
    elif iotType == 2:
        resultadoBio = 2
        posicao = resultPosition


'''
Verifica se ocorreu uma tentativa de acesso biometrico
Return:0 -> Não ocorreu tentativa / leitor em modo de cadastro
Return:-1 -> Nenhuma digital encontrada para a tentativa

Success:1 -> Acesso a sala dedicada
Success:2 -> Acesso a sala de reuniao

func:0 -> Tentativa realizada por um usuario
func:1 -> Tentativa realizada por um funcionario

permissao:0 -> Acesso a unidade negado
permissao:1 -> Acesso a unidade permitido

@app.route('/_verificar_entrada_biometria')
def verificarEntradaBiometrica():
    global resultadoBio
    global posicao
    global modo
    acesso = 0
    try:
        valor, statusC, desconto, statusD = VerificarCOnfiguracoesSigv()
        if resultadoBio == 0 or modo == 'Cadastro':
            resultadoBio = 0
            return jsonify(0)
        elif resultadoBio == -1:
            resultadoBio = 0
            return jsonify(-1)
        nome, cod, tipo = identificarEntradaBio(posicao)
        if resultadoBio == 1:
            resultadoBio = 0
            if cod != -1 and cod is not None:
                if tipo == 'F':
                    if cod in listaPermissoesFuncionario:
                        acesso = 1
                    resultado = {'success': 1, 'nome': nome, 'permissao': acesso}
                else:
                    for user in listaPermissoes:
                        if int(user.cod) == int(cod):
                            acesso = 1
                    if tipo == 'Y' and int(statusC) == 1:
                        if iotID == 1565:
                            cred = VerificarCreditos(int(cod), valor)
                            if cred == 0:
                                resultado = {'success': 3, 'nome': nome, 'permissao': 0,
                                             'msg': 'Você não possui créditos para acessar essa unidade.'}
                            elif cred == 1:
                                resultado = {'success': 3, 'nome': nome, 'permissao': acesso, 'msg': 'R$' + str(
                                    valor) + ' foram utilizados. Ainda é possuvel utilizalos até o fim do dia.'}
                            else:
                                resultado = {'success': 3, 'nome': nome, 'permissao': acesso, 'msg': 'Bem Vindo!'}
                        else:
                            resultado = {'success': 1, 'nome': nome, 'permissao': 0}
                    else:
                        resultado = {'success': 1, 'nome': nome, 'permissao': acesso}
                return jsonify(resultado)
            else:
                return jsonify(0)
        elif resultadoBio == 2:
            resultadoBio = 0
            if tipo == 'F':
                if cod in listaPermissoesFuncionario:
                    acesso = 1
                resultado = {'success': 2, 'cod': cod, 'nome': nome, 'func': 1, 'permissao': acesso}
            elif tipo == 'Y':
                resultado = {'success': 2, 'cod': cod, 'nome': nome, 'func': 0, 'permissao': 0}
            else:
                for user in listaPermissoes:
                    if user.cod == cod or int(user.cod) == int(cod):
                        acesso = 1
                resultado = {'success': 2, 'cod': cod, 'nome': nome, 'func': 0, 'permissao': acesso}
            return jsonify(resultado)
    except Exception as e:
        registrarLogErro(e)
        return jsonify(0)
'''

'''
Verifica se o usuario possui créditos suficientes para acesso.
0 -> Creditos insuficientes
1 -> Tem creditos para acessar
2 -> Ja possui créditos para essa data

def VerificarCreditos(cod, valor):
    try:
        creditoDia = requests.get(
            "http://alugueap.com/userpweb/api/rasp/verificar-credito-temporario-dia.php?idUsuario=" + str(cod)).text
        if creditoDia != "-1":
            return 2

        dados = requests.get("http://alugueap.com/userpweb/api/rasp/get-dados-usuario-club.php?idUsuario=" + str(cod))
        cred = float(json.loads(dados.text)['Credito'])
        clubID = int(json.loads(dados.text)['Ydc'])
        pin = json.loads(dados.text)['pin']
        if float(cred) < float(valor):
            return 0
        return int(requests.get("http://alugueap.com/userpweb/api/rasp/adicionar-credito-temporario.php?ClubId=" + str(
            clubID) + "&pin=" + str(pin) + "&valor=" + str(valor)).text)
    except Exception as e:
        registrarLogErro(e)
        return 0
'''

def VerificarCOnfiguracoesSigv():
    conn = sqlite3.connect(sqlitePath)
    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT * FROM sigvConfig;")
    row = cursorLocal.fetchone()
    valor = float(row[0])
    statusC = int(row[1])
    desconto = int(row[2])
    statusD = int(row[3])
    conn.close()
    return valor, statusC, desconto, statusD


# Verifica qual o tipo do IOT
@app.route('/_check_iot_type')
def checkIotType():
    global iotType
    # TODO Verificar qual atributo identifica o tipo de sala
    if int(iotID) < 1000:
        iotType = 2
    elif int(iotID) > 999:
        iotType = 1
    return jsonify(iotType)


'''
Reinicia os registros que vinculam a posição da digital com o usuario/funcionario
'''
def resetPosicoesBio():
    try:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        sql = "UPDATE usuarioPosicao SET pd0 = null, pd1 = null;"
        cursor.execute(sql)
        sql = "UPDATE funcionarioPosicao SET pd0 = null, pd1 = null;"
        cursor.execute(sql)
        conn.commit()

        conn.close()
    except Exception as e:
        registrarLogErro(e)


'''
Atualiza os registros de permissoes local
'''
def atualizar_permissoes_local():
    ids = []
    try:
        for user in listaPermissoes:
            acesso = {'id': 0, 'cod': user.cod}
            ids.append(acesso)
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("DELETE FROM acesso;")
        conn.commit()
        cursorLocal.executemany("INSERT OR REPLACE INTO acesso VALUES(:id,:cod);", ids)
        conn.commit()

        conn.close()
    except Exception as e:
        registrarLogErro(e)


'''
Atualiza a lista de permissoes com base nos registros locais
'''
def carregar_permissoes_local():
    try:
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute(
            "SELECT user.id, user.nome, user.pin, '--' FROM usuario user INNER JOIN acesso a on a.cliente = user.id;")
        for row in cursorLocal.fetchall():
            usuario = Usuario(row[1], row[0], row[2], row[3])
            listaPermissoes.append(usuario)
        get_admin()
        get_Comercial()
        conn.close()
    except Exception as e:
        registrarLogErro(e)


'''
Identifica o usuario que tentou realizar acesso biometrico
'''
def identificarEntradaBio(position):
    nome = ''
    cod = -1
    tipo = ''
    try:
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute(
            "SELECT usu.nome, usu.id, usu.tipo FROM usuario usu INNER JOIN usuarioPosicao posi on posi.idUsuario = usu.id WHERE posi.pd0 = ? or posi.pd1 = ?;",
            (position, position))
        for row in cursorLocal.fetchall():
            nome = row[0]
            cod = row[1]
            tipo = row[2]
        if cod == -1 or cod is None:
            cursorLocal.execute(
                "SELECT nome, id, 'F' FROM funcionario func INNER JOIN funcionarioPosicao posi on posi.idFuncionario = func.id WHERE posi.pd0 = ? or posi.pd1 = ?;",
                (position, position))
            for row in cursorLocal.fetchall():
                nome = row[0]
                cod = row[1]
                tipo = row[2]
        conn.close()
        return nome, cod, tipo
    except Exception as e:
        registrarLogErro(e)
        return nome, cod, tipo


'''
Verifica os dados do usuario administrador
'''
def get_admin():
    global Admin
    cursor = None
    cnx = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "select user.idusuario as Cod,'Admin' as Nome, p.pin as Pin, user.usuario as Email from tab_ydo_usuario user inner join tab_ydo_pin_usuario p on p.idusuario = user.idusuario WHERE user.tipousuario= 'A';")

        result_set = cursor.fetchall()
        for row in result_set:
            nome = row['Nome']
            cod = row['Cod']
            pin = row['Pin']
            email = row['Email']
            Admin = Usuario(str(nome), str(cod), str(pin), str(email))

        atualizarAdminLocal()
    except Exception as e:
        registrarLogErro(str(e))
        carregarAdminLocal()
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()


def atualizarAdminLocal():
    global Admin
    try:
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("DELETE FROM adm;")
        conn.commit()
        cursorLocal.execute("INSERT OR REPLACE INTO adm VALUES(?,?,?);", (Admin.cod, Admin.nome, Admin.pin))
        conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(e)


def carregarAdminLocal():
    global Admin
    try:
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("SELECT id, nome, pin FROM adm;")
        for row in cursorLocal.fetchall():
            Admin.cod = row[0]
            Admin.nome = row[1]
            Admin.pin = row[2]
        conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(e)


'''
Verifica os dados do usuario Comercial
'''
def get_Comercial():
    cursor = None
    cnx = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "select user.idusuario as Cod,'Comercial YouDO' as Nome, p.pin as Pin, user.usuario as Email from tab_ydo_usuario user inner join tab_ydo_pin_usuario p on p.idusuario = user.idusuario WHERE user.idusuario = 3;")

        result_set = cursor.fetchall()
        for row in result_set:
            nome = row['Nome']
            cod = row['Cod']
            pin = row['Pin']
            email = row['Email']
            Comercial = Usuario(str(nome), str(cod), str(pin), str(email))
            listaPermissoes.append(Comercial)

    except Exception as e:
        registrarLogErro(str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()


def getTipoUsuario(cod):
    tipo = ''
    conn = sqlite3.connect(sqlitePath)
    cursor = conn.cursor()
    sql = "SELECT tipo FROM usuario WHERE id = ?;"
    cursor.execute(sql, (cod,))
    for row in cursor.fetchall():
        tipo = row[0]
    conn.close()
    return tipo

# =================================== Agendamentos START ===================================#

'''
Realiza uma tentativa de realizar o agendamento
'''
@app.route('/_agendar')
def agendar():
    inicio = request.args.get('inicio')
    final = request.args.get('final')
    iot = request.args.get('iot')
    descricao = request.args.get('descricao')
    usuario = request.args.get('usuario')
    contrato = request.args.get('contrato')
    try:
        agendamento = requests.get(iotUrl + "?classe=IOT&metodo=AgendarHorario&atributo[datainicio]=" + str(
            inicio) + "&atributo[datafim]=" + str(final) + "&atributo[iotid]=" + str(
            iot) + "&atributo[descricao]=" + str(descricao) + "&atributo[idusuario]=" + str(
            usuario) + "&atributo[codlocacontrato]=" + str(contrato))
        return jsonify(json.loads(agendamento.text))
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(0)


'''
Valida o pin informado para iniciar o processo de agendamento
'''
@app.route('/_validate_pin_agendamento')
def validadePinAgendamento():
    pin = request.args.get('pin')
    usuario = {'cod': 0, 'user': 0, 'result': 0}
    conn = sqlite3.connect(sqlitePath)

    cursorLocal = conn.cursor()
    cursorLocal.execute("SELECT id, nome FROM usuario WHERE pin = '" + str(pin) + "';")

    for row in cursorLocal.fetchall():
        usuario = {'cod': row[0], 'user': row[1], 'result': 1}
    conn.close()
    return jsonify(usuario)


'''
Retorna as salas de reunião para agendamento
'''
@app.route('/_get_salas_reuniao')
def get_salas_reuniao():
    cursor = None
    cnx = None
    salas = []
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset=charset,
                              cursorclass=pymysql.cursors.DictCursor)

        cursor = cnx.cursor()
        cursor.execute(
            "select descricao as sala, iotid as iotID from tab_ydo_servico_empreendimento where iotid in (222,333)")

        result_set = cursor.fetchall()
        for row in result_set:
            idiot = row['iotID']
            nome = row['sala']
            sala = {'nome': nome, 'iot': idiot, 'iotAtual': iotID}
            salas.append(sala)
    except Exception as e:
        registrarLogErro(str(e))
        return jsonify(salas)
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return jsonify(salas)


# Identifica se o pin informado é referente a um cliente ou hospede, retornando seus respectivos contratos
@app.route('/_contratos')
def get_contratos():
    usuario = request.args.get('usuario')
    contratos = []
    cursor = None
    cnx = None
    tipo = None
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset='utf8mb4',
                              cursorclass=pymysql.cursors.DictCursor)
        cursor = cnx.cursor()
        cursor.execute("select tipousuario from tab_ydo_usuario WHERE idusuario =%s", [usuario])
        result_set = cursor.fetchall()
        for row in result_set:
            tipo = row['tipousuario']

        if tipo == 'C':
            contratos = get_contratos_cliente(usuario)
        elif tipo == 'H':
            contratos = get_contratos_hospedes(usuario)
    except Exception as e:
        registrarLogErro('Get Contratos - ' + str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return jsonify(contratos)


# Retorna os contratos de um cliente
def get_contratos_cliente(usuario):
    contratos = []
    cursor = None
    cnx = None
    idcliente = 0
    try:
        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset='utf8mb4',
                              cursorclass=pymysql.cursors.DictCursor)
        cursor = cnx.cursor()
        cursor.execute(
            "select c.codcliente from tb_clientes c inner join tab_ydo_usuario u on u.idcliente = c.codcliente where u.idusuario =%s",
            [usuario])
        result_set = cursor.fetchall()
        for row in result_set:
            idcliente = row['codcliente']

        cursor.execute(
            "select con.codlocacontrato as Cod, plano.lp_nome as Nome from tb_loca_contrato con inner join tb_loca_contrato_valor valor on valor.codlocacontrato = con.codlocacontrato inner join tb_loca_plano_periodo_preco ppp on ppp.lppp_id = valor.idplanoperiodopreco inner join tb_loca_plano plano on plano.lp_id = ppp.lp_id where con.status = 'Vigente' and con.cliente = %s",
            [idcliente])
        result_set = cursor.fetchall()
        for row in result_set:
            contratos.append({'cod': row['Cod'], 'desc': row['Nome']})

    except Exception as e:
        registrarLogErro('Get Contrato Clientes - ' + str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return contratos


# Retorna os contratos de um hospede
def get_contratos_hospedes(usuario):
    contratos = []
    cursor = None
    cnx = None
    contrato = 0
    try:
        conn = sqlite3.connect(sqlitePath)
        cursorLocal = conn.cursor()
        cursorLocal.execute("SELECT contrato FROM usuario WHERE id = ?;", (str(usuario),))
        for row in cursorLocal.fetchall():
            contrato = row[0]
        conn.close()

        cnx = pymysql.connect(host=ipbanco, user=userbanco, password=passbanco, db=db, charset='utf8mb4',
                              cursorclass=pymysql.cursors.DictCursor)
        cursor = cnx.cursor()
        cursor.execute(
            "select plano.lp_nome plano from tb_loca_contrato_valor valor inner join tb_loca_plano_periodo_preco ppp on ppp.lppp_id = valor.idplanoperiodopreco inner join tb_loca_plano plano on plano.lp_id = ppp.lp_id where valor.codlocacontrato = " + str(
                contrato) + ";")
        result_set = cursor.fetchall()
        for row in result_set:
            contratos.append({'cod': contrato, 'desc': row['plano']})
    except Exception as e:
        registrarLogErro('Get Contrato Hospede - ' + str(e))
    finally:
        if cursor is not None:
            cursor.close()
        if cnx is not None:
            cnx.close()
    return contratos


# Inicializa o uso da sala caso o pin informado esteja no prazo permitido de uso. Caso a reunião não esteja iniciada, é enviado um chamado para api para registrar o inicio, caso contrario é apenas alterada localmente para "em andamento"
@app.route('/_inciar_agendamento')
def iniciar_reuniao():
    global proximoAgendamento
    global listaPermissoes
    cod = request.args.get('cod')
    status = 1
    if datetime.datetime.now() < datetime.datetime.strptime(proximoAgendamento.inicio,
                                                            '%Y-%m-%d %H:%M:%S') + relativedelta(minutes=-10):
        result = {'success': 0}
        return jsonify(result)

    if proximoAgendamento.andamento != 1:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        sql = "UPDATE agendamento SET andamento = 1 WHERE id = ?;"
        cursor.execute(sql, (proximoAgendamento.id,))
        conn.commit()

        conn.close()
        proximoAgendamento.andamento = 1
        status = 0

    if proximoAgendamento.iniciado != 1:
        try:
            requests.get(iotUrl + "?classe=IOT&metodo=IniciarUso&atributo[idagendaservico]=" + str(
                proximoAgendamento.id) + "&atributo[datainicio]=" + str(
                datetime.datetime.now()) + "&atributo[idusuario]=" + str(listaPermissoes[0].cod))
        except Exception as e:
            conn = sqlite3.connect(sqlitePath)
            cursor = conn.cursor()
            cursor.execute("INSERT INTO registroinicio VALUES(?,?,?)",
                           (proximoAgendamento.id, cod, datetime.datetime.now()))
            conn.commit()
            conn.close()
            registrarLogErro(str(e))
        proximoAgendamento.iniciado = 1
        proximoAgendamento.andamento = 1
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        sql = "UPDATE agendamento SET iniciouso = 1, andamento = 1 WHERE id = ?;"
        cursor.execute(sql, (proximoAgendamento.id,))
        conn.commit()

        conn.close()
    result = {'success': 1, 'status': status}
    return jsonify(result)


# Realiza o encerramento da reunião atual
@app.route('/_encerrar_reuniao')
def encerrar_reuniao():
    global proximoAgendamento
    global listaPermissoes
    cod = request.args.get('cod')
    try:
        requests.get(iotUrl + "?classe=IOT&metodo=FinalizarUso&atributo[idagendaservico]=" + str(
            proximoAgendamento.id) + "&atributo[datafim]=" + str(
            datetime.datetime.now()) + "&atributo[idusuario]=" + str(listaPermissoes[0].cod))
    except Exception as e:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        cursor.execute("INSERT INTO registroencerramento VALUES(?,?,?);",
                       (proximoAgendamento.id, cod, datetime.datetime.now()))
        conn.commit()
        conn.close()
        print(e)

    proximoAgendamento.encerrado = 1
    proximoAgendamento.andamento = 0

    conn = sqlite3.connect(sqlitePath)

    cursor = conn.cursor()

    sql = "UPDATE agendamento SET fimuso = 1, andamento = 0 WHERE id = ?;"
    cursor.execute(sql, (proximoAgendamento.id,))
    conn.commit()

    conn.close()

    return jsonify(1)


# Atualiza os registros de inicializações de reunião que ocorreram durante um periodo em que o IOT esteve offline
def atualizar_registros_inicio():
    conn = sqlite3.connect(sqlitePath)

    cursor = conn.cursor()

    sql = "SELECT * FROM registroinicio;"
    cursor.execute(sql)
    for row in cursor.fetchall():
        try:
            requests.get(iotUrl + "?classe=IOT&metodo=IniciarUso&atributo[idagendaservico]=" + str(
                row[0]) + "&atributo[datainicio]=" + str(row[2]) + "&atributo[idusuario]=" + str(row[1]))
            cursor.execute("DELETE FROM registroinicio WHERE id = ?", (row[0],))
        except Exception as e:
            registrarLogErro(str(e))
    conn.close()


# Atualiza os registros de encerramento de reunião que ocorreram durante um periodo em que o IOT esteve offline
def atualizar_registros_encerramento():
    conn = sqlite3.connect(sqlitePath)

    cursor = conn.cursor()

    sql = "SELECT * FROM registroencerramento;"
    cursor.execute(sql)
    for row in cursor.fetchall():
        try:
            requests.get(iotUrl + "?classe=IOT&metodo=FinalizarUso&atributo[idagendaservico]=" + str(
                row[0]) + "&atributo[datainicio]=" + str(row[2]) + "&atributo[idusuario]=" + str(row[1]))
            cursor.execute("DELETE FROM registroencerramento WHERE id = ?", (row[0],))
        except Exception as e:
            registrarLogErro(str(e))
    conn.close()


# Inicia atualização dos registros de quanto o IOT esteve offline
def atualizar_prod_registros_off():
    atualizar_registros_inicio()
    atualizar_registros_encerramento()


# Identifica qual o proximo agendamento a ser exibido
def get_agendamento_dia():
    global iotID
    global proximoAgendamento
    conn = sqlite3.connect(sqlitePath)

    cursor = conn.cursor()

    sql = "SELECT agen.id, usu.nome, agen.inicio, agen.fim, agen.iotid, agen.iniciouso, agen.fimuso, agen.andamento, agen.contrato FROM agendamento agen inner join usuario usu on usu.id = agen.cliente WHERE date(agen.inicio) = date('now') and (agen.fimuso = 0 or agen.andamento != 0) and (strftime('%s', agen.fim) > strftime('%s', DATETIME('now', 'localtime')) or agen.andamento != 0) and agen.iotid = " + str(
        iotID) + " order by abs(strftime('%s', DATETIME('now', 'localtime')) - strftime('%s', agen.fim)) limit 1;"
    cursor.execute(sql)
    for row in cursor.fetchall():
        proximoAgendamento = Agendamento(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8])
        conn.close()
        get_permissoes_reuniao()
        return
    proximoAgendamento = Agendamento(-1, 'Sem Agendamentos', ' -- ', ' -- ', iotID, 0, 1, 0, 0)


'''
Verifica se é necessario atualizar qual é o proximo agendamento para a sala
'''
@app.route('/_verificar_agendamento')
def VerificarAgendamento():
    global proximoAgendamento
    currentDay = str(datetime.date.today().year) + '-' + str(datetime.date.month) + '-' + str(datetime.date.day)
    extrapolado = 0
    if proximoAgendamento.id == -1 or proximoAgendamento.iniciado == 0:
        get_agendamento_dia()
    elif proximoAgendamento.encerrado == 1 and datetime.datetime.strptime(proximoAgendamento.fim,
                                                                          '%Y-%m-%d %H:%M:%S') < datetime.datetime.now() and proximoAgendamento.andamento != 1 and proximoAgendamento.id != -1:
        get_agendamento_dia()
    elif (datetime.datetime.strptime(currentDay + " 00:00:00",
                                     '%Y-%m-%d %H:%M:%S') > datetime.datetime.now() > datetime.datetime.strptime(
        currentDay + " 00:05:00", '%Y-%m-%d %H:%M:%S') and proximoAgendamento.id != -1) or (
            proximoAgendamento.iniciado != 1 and datetime.datetime.strptime(proximoAgendamento.fim,
                                                                            '%Y-%m-%d %H:%M:%S') < datetime.datetime.now() and proximoAgendamento.andamento != 1):
        encerrar_reuniao()
        get_agendamento_dia()
    if proximoAgendamento.id != -1 and datetime.datetime.strptime(proximoAgendamento.fim,
                                                                  '%Y-%m-%d %H:%M:%S') < datetime.datetime.now():
        extrapolado = 1
    agendamento = {'responsavel': proximoAgendamento.responsavel,
                   'inicio': proximoAgendamento.inicio.split(' ', 1)[1][:-3],
                   'fim': proximoAgendamento.fim.split(' ', 1)[1][:-3], 'iniciou': proximoAgendamento.iniciado,
                   'encerrou': proximoAgendamento.encerrado, 'andamento': proximoAgendamento.andamento,
                   'extrapolado': extrapolado, 'contrato': proximoAgendamento.contrato}
    return jsonify(agendamento)


'''
Atualiza a lista de permissoes de acordo com a proxima reuniao
'''
def get_permissoes_reuniao():
    global listaPermissoes
    if iotType == 2:
        listaPermissoes.clear()
        conn = sqlite3.connect(sqlitePath)

        cursor = conn.cursor()

        sql = "SELECT user.nome, user.id, user.pin, 'email' FROM usuario user INNER JOIN agendamento agen on agen.contrato = user.contrato WHERE agen.id = ?;"
        cursor.execute(sql, (proximoAgendamento.id,))
        for row in cursor.fetchall():
            nome = row[0]
            cod = row[1]
            pin = row[2]
            email = row[3]
            usuario = Usuario(str(nome), str(cod), str(pin), str(email))
            listaPermissoes.append(usuario)
        conn.close()

        atualizar_permissoes_local()


# ===================================  Agendamentos END  ===================================#

if __name__ == '__main__':
    app.run()
