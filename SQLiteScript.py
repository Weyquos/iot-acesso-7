import datetime
import sqlite3
import time

import requests
from flask import json

#sqlitePath = "/home/fabiano/Files/Dev_YouDO/iotyoudo.db"
#sqlitePath = "/home/pi/iotyoudo.db"
sqlitePath = "/home/youdo/YouDO_Dev/iotyoudo.db"

def get_siv_configs():
    try:
        configs = requests.get("http://alugueap.com/userpweb/api/rasp/get-sigv-config.php")
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        cursor.execute("DELETE FROM sigvConfig;")
        conn.commit()

        valor = float(json.loads(configs.text)['Valor'])
        statusC = int(json.loads(configs.text)['StatusConsumacao'])
        desconto = int(json.loads(configs.text)['Desconto'])
        statusD = int(json.loads(configs.text)['StatusDesconto'])
        cursor.execute("INSERT INTO sigvConfig VALUES(?,?,?,?);", (valor, statusC, desconto, statusD))
        conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))

def get_funcionarios_API():
    try:
        permissoes = requests.get("http://api.youdobrasil.com.br?classe=IOT&metodo=listarFuncionarios")
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        for user in json.loads(permissoes.text)['list']:
            cod = user['Cod']
            nome = user['Nome']
            d0 = user['d0']
            d1 = user['d1']
            cursor.execute("INSERT OR REPLACE INTO funcionario VALUES(?,?,?,?);", (cod, nome, d0, d1))
            conn.commit()
        conn.close()
    except Exception as e:
        registrarLogErro(str(e))

def get_usuarios():
    try:
        usuarios = requests.get("http://alugueap.com/userpweb/api/rasp/get-usuarios-iot.php")
        return json.loads(usuarios.text)
    except Exception as e:
        registrarLogErro(str(e))
        return 0

def get_agendamentos():
    try:
        usuarios = requests.get("http://alugueap.com/userpweb/api/rasp/get-agendamentos-dia.php")
        return json.loads(usuarios.text)
    except Exception as e:
        registrarLogErro(str(e))
        return 0

def set_usuarios():
    try:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        clientes = get_usuarios()
        if clientes != 0:
            cursor.executemany("INSERT OR REPLACE INTO usuario VALUES(:Cod,:Nome,:Pin,:Tipo,:d0,:d1,:Contrato);", get_usuarios())
            conn.commit()
            conn.close()
    except Exception as e:
        registrarLogErro(str(e))

def set_agendamentos():
    try:
        agendamentos = get_agendamentos()
        if agendamentos != 0:
            conn = sqlite3.connect(sqlitePath)
            cursor = conn.cursor()
            cursor.execute("DELETE FROM agendamento;")
            conn.commit()
            cursor.executemany("INSERT OR REPLACE INTO agendamento VALUES(:id,:usuario,:inicio,:fim,:iot,:iniciouso,:fimuso,:andamento,:Contrato);", agendamentos)
            conn.commit()
            conn.close()
    except Exception as e:
        registrarLogErro(str(e))

def registrarLogErro(e):
    try:
        conn = sqlite3.connect(sqlitePath)
        cursor = conn.cursor()
        sql = "INSERT INTO log(erro, hora) VALUES (?,?)"
        cursor.execute(sql, (e, datetime.datetime.now()))
        conn.commit()
        conn.close()
    except Exception as e:
        print(e)

con = sqlite3.connect(sqlitePath)
c = con.cursor()
c.execute("CREATE TABLE IF NOT EXISTS usuario(id INTEGER PRIMARY KEY, nome TEXT, pin CHAR, tipo CHAR,d0 BLOB, d1 BLOB, contrato INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS funcionario(id INTEGER PRIMARY KEY, nome TEXT, d0 BLOB, d1 BLOB);")
c.execute("CREATE TABLE IF NOT EXISTS usuarioPosicao(idUsuario INTEGER PRIMARY KEY, pd0 INTEGER, pd1 INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS funcionarioPosicao(idFuncionario INTEGER PRIMARY KEY, pd0 INTEGER, pd1 INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS statusAgendamento(id INTEGER, iniciouso INTEGER, fimuso INTEGER, andamento INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS agendamento(id INTEGER PRIMARY KEY, cliente INTEGER, inicio DATETIME, fim DATETIME, iotid INTEGER,iniciouso INTEGER, fimuso INTEGER, andamento INTEGER, contrato INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS acesso(id INTEGER, cliente INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS acessoFuncionario(id INTEGER, funcionario INTEGER);")
c.execute("CREATE TABLE IF NOT EXISTS configuracao(iotID INTEGER, tempoAbertura INTEGER, updateHora INTEGER, updateMinuto INTEGER, ip CHAR, userBanco CHAR, passBanco CHAR, database CHAR, charset CHAR);")
c.execute("CREATE TABLE IF NOT EXISTS registroinicio(id INTEGER, cliente INTEGER, hora DATETIME);")
c.execute("CREATE TABLE IF NOT EXISTS registroencerramento(id INTEGER, cliente INTEGER, hora DATETIME);")
c.execute("CREATE TABLE IF NOT EXISTS empresa(id INTEGER,nome TEXT);")
c.execute("CREATE TABLE IF NOT EXISTS log(id INTEGER PRIMARY KEY AUTOINCREMENT, hora DATETIME,erro TEXT);")
c.execute("CREATE TABLE IF NOT EXISTS adm(id INTEGER, nome TEXT, pin TEXT);")
c.execute("CREATE TABLE IF NOT EXISTS sigvConfig(valor REAL, statusConsumacao INTEGER, desconto INTEGER, statusDesconto INTEGER);")
con.close()

while True:
    try:
        time.sleep(2)
        set_usuarios()
        time.sleep(10)
        get_siv_configs()
        time.sleep(10)
        get_funcionarios_API()
        time.sleep(10)
        set_agendamentos()
    except Exception as erro:
        registrarLogErro(str(erro))
    time.sleep(300)
